import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class SAXParser extends DefaultHandler {
    private String thisElement;
    private Student stud = new Student();
    public static void main(String[] args) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser = null;
        try {
            parser = factory.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        SAXParser saxp = new SAXParser();

        try {
            parser.parse(new File("resources/univer.xml"), saxp);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start parse XML...");
    }

    @Override
    public void startElement(String namespaceURI,
                            String localname,
                            String qName,
                            Attributes attr) throws SAXException{
        thisElement = qName;
    }

    @Override
    public void characters(char[]ch,
                           int start,
                           int length){
        if (thisElement.equals("age")){
            stud.setAge(new Integer(new String(ch,start,length)));
            System.out.println("age: " + new String(ch,start,length));
        }
        if (thisElement.equals("lastname")){
            stud.setLastname(new String(ch,start,length));
            System.out.println("lastname: " + new String(ch,start,length));
        }
        if (thisElement.equals("name")){
            stud.setName(new String(ch,start,length));
            System.out.println("name: " + new String(ch,start,length));
        }
    }

    @Override
    public void endElement(String namespaceURI,
                           String location,
                           String qName){
        thisElement = "";
    }

    @Override
    public void endDocument(){
        System.out.println("End pars XML...");
    }
}
